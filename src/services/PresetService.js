export class PresetService {

    presets = [
        {
            name: "My Subreddits",
            subreddits: ["__LOCALSTORE__"]
        },
        {
            name: "Mystery",
            subreddits: [
                "rbi",
                "unsolvedmysteries",
                "whatisthisthing",
            ]
        },
        {
            name: "App Development",
            subreddits: [
                "flutterdev",
                "reactnative",
                "ionic",
                "androiddev",
                "iosprogramming",
                "iosdev",
                "swift",
            ]
        },
        {
            name: "Web Development",
            subreddits: [
                "webdev",
                "web_design",
                "php",
                "laravel",
                "elm",
                "reactjs",
                "vuejs",
                "angular",
            ]
        },
        {
            name: "Programming Languages",
            subreddits: [
                "dartlang",
                "csharp",
                "fsharp",
                "elixir",
                "phoenixframework",
                "typescript",
                "dotnet",
                "java",
            ]
        },
        {
            name: "Music Production",
            subreddits: [
                "tranceproduction",
                "edmproduction",
                "harddanceproduction",
                "ableton",
                "fl_studio",
                "logic_studio",
            ]
        },
        {
            name: "Hacking",
            subreddits: [
                "netsec",
                "hacking",
                "blackhat",
                "regames",
                "reverseengineering",
                "defcon",
                "malware",
            ]
        },
        {
            name: "Misc",
            subreddits: [
                "whatcouldgowrong",
                "wtf",
                "hardstyle",
                "explainlikeimfive",
                "programmerhumor",
                "starterpacks",
            ]
        },
    ];

    getPresetNames() {
        return this.presets.map(p => p.name);
    }

    getPresetSubreddits(name) {
        return this.presets.find(p => p.name === name).subreddits;
    }
}